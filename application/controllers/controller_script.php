<?php

class Controller_script extends Controller
{
	function __construct()
	{
		$this->model = new Model_script();
		$this->view = new View();
	}
	function action_index()
	{	
		
	}
	function action_getPage(){ //получение страниц
		$sort = null;
		if (isset($_POST['sort'])) {
			$sort = $_POST['sort'];
		}
		$data = $this->model->GetPage($_POST['page'] - 1, $sort);	
		$this->view->generate('js_view.php', $data);
	}
	function action_setTask(){ //вставка задач
		$this->model->addTask($_POST['name'], $_POST['mail'], $_POST['description']);
		$pageInfo = $this->model->pageCount();
		$response = [
			'pageCount' => $pageInfo
		];
		$this->view->generate('js_view.php', $response);// отрисовка страницу
	}
	function action_login(){//авторизация
		$hash = 0;
		$id = 0;
		$login = $_POST['login'];
		$password = $_POST['password'];
		$account = $this->model->getLogin($login);
		if (!empty($account)) {
			if($account['password'] === md5(md5($password))){
				$id = $account['id'];
				$hash = $this->model->updateHash($id);
				$result = 'ok';
			}else{
				$result = 'bad';
			}
		}else{
			$result = 'bad';
		}
		
		$response = [
			'result' => $result,
			'hash' => $hash,
			'id' => $id
		];
		$this->view->generate('js_view.php', $response);
	}
	function action_logout(){ //выход из акк
		$login = $this->model->getLoginFromId($_POST['id']);
		if (!empty($login)) {
				$id = $login['id'];
				$this->model->updateHash($id);
				$result = 'ok';
			}else{
				$result = 'bad';
			}		
		$response = [
			'result' => $result,
			'id' => $id
		];
		$this->view->generate('js_view.php', $response);
	}
	public function action_getHash() //получить хеш код
	{
		$result = 'bad';
		$hash = '';
		$login = $this->model->getLoginFromId($_POST['id']);
		if (!empty($login)) {
			$result = 'ok';
			$hash = $login['hash'];
		}
		$response = [
			'result' => $result,
			'hash' => $hash,
		];
		$this->view->generate('js_view.php', $response);
	}
	public function action_completed() // пометить как выполненое
	{
		$result = 'bad';
		if (isset($_POST['hash'])) {
			$account = $this->model->getLoginFromHash($_POST['hash']);
			if (!empty($account)) {
				$this->model->updateCompleted($_POST['id']);
				$result = 'ok';
			}
		}
		$response = [
			'result' => $result,
		];
		$this->view->generate('js_view.php', $response);
	}
	public function action_editTask()// изменить задачу
	{
		$result = 'bad';
		if (isset($_POST['hash'])) {
			$account = $this->model->getLoginFromHash($_POST['hash']);
			if (!empty($account)) {
				$this->model->updateTask($_POST['id'], $_POST['name'], $_POST['mail'], $_POST['description']);
				$result = 'ok';
			}
		}
		$response = [
			'result' => $result,
		];
		$this->view->generate('js_view.php', $response);
	}
	
}