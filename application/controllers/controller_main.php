<?php

class Controller_Main extends Controller
{
	function __construct()
	{
		$this->model = new Model_main();
		$this->view = new View();
	}
	function action_index()
	{	
		$authorized = false;
		if (isset($_COOKIE['id']) && isset($_COOKIE['hash'])) { // проверка на авторизацию
			$login = $this->model->getLoginFromId($_COOKIE['id']);
			if (!empty($login) && $login['hash'] == $_COOKIE['hash']) {
				$authorized = true;
			}else{
				setcookie("id", "", time() - 3600*24*30*12, "/");
        		setcookie("hash", "", time() - 3600*24*30*12, "/");
				
			}
		}
		$task = $this->model->GetFerstPageWithTask();
		$pageInfo = $this->model->pageCount();
		$response = [
			'task' => $task,
			'pageCount' => $pageInfo,
			'authorized'=> $authorized
		];
		$this->view->generate('main_view.php', $response);
	}
}