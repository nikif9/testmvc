<?php

class Model_main extends Model
{
	public function GetFerstPageWithTask(){ // отрисовка страницы
		$offset = 0;
		$query = $this->getFromTable('task LIMIT :offset, :count');
		$query->bindValue(':count', self::COUNT, PDO::PARAM_INT);
		$query->bindValue(':offset', $offset, PDO::PARAM_INT);
		$query->execute();
		return $query->fetchAll();
	}
	public function getLoginFromId($id) // получение пользователя по ид
	{
		$sql = 'login WHERE id = :id';
		$query = $this->getFromTable($sql);
		$query->bindParam(':id', $id);
		$query->execute();
		return $query->fetch();
	}
}
