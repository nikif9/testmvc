<?php

class Model_script extends Model
{
	public function GetPage($pageNum, $sort){ // получение страниацы с отпределеной сортировкой
		$sql = 'task ';
		switch ($sort) {
			case 'nameSortASC':
				$sql .= 'ORDER BY name ASC ';
				break;
			case 'nameSortDESC':
				$sql .= 'ORDER BY name DESC ';
				break;

			case 'mailSortASC':
				$sql .= 'ORDER BY email ASC ';
				break;
			case 'mailSortDESC':
				$sql .= 'ORDER BY email DESC ';
				break;

			case 'statusSortASC':
				$sql .= 'ORDER BY status ASC ';
				break;
			case 'statusSortDESC':
				$sql .= 'ORDER BY status DESC ';
				break;

			default:
				$sql = 'task ';
				break;
		}
		$sql .= 'LIMIT :offset, :count';
		$query = $this->getFromTable($sql);
		$query->bindValue(':count', self::COUNT, PDO::PARAM_INT);
		$query->bindValue(':offset', self::COUNT * $pageNum, PDO::PARAM_INT);
		$query->execute();
		return $query->fetchAll();
	}
	public function getLogin($login) // поискк по именим пользователя
	{
		$sql = 'login WHERE login = :login LIMIT 1';
		$query = $this->getFromTable($sql);
		$query->bindParam(':login', $login);
		$query->execute();
		return $query->fetch();
	}
	public function updateHash($id) // обновление hash кода
	{
		$sql = 'login set hash = :hash WHERE id = :id';
		$hash = $this->generateCode(10);
		$query = $this->update($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':hash', $hash);
		$query->execute();
		return $hash;
	}
	public function addTask($name, $mail, $description) // добавить задачу
	{
		$query = $this->insert('task (name, email, description) VALUES (:name, :email, :description)');
		$query->bindParam(':name', $name);
		$query->bindParam(':email', $mail);
		$query->bindParam(':description', $description);
		$query->execute();
	}
	public function getLoginFromId($id) // поиск пользователя по ид
	{
		$sql = 'login WHERE id = :id';
		$query = $this->getFromTable($sql);
		$query->bindParam(':id', $id);
		$query->execute();
		return $query->fetch();
	}
	public function getLoginFromHash($hash) // поиско пользователя по hash коду
	{
		$sql = 'login WHERE hash = :hash';
		$query = $this->getFromTable($sql);
		$query->bindParam(':hash', $hash);
		$query->execute();
		return $query->fetch();
	}
	public function updateCompleted($id) // поставить как выполнено
	{
		$sql = 'task set status = :status WHERE id = :id';
		$query = $this->update($sql);
		$query->bindParam(':id', $id);
		$query->bindValue(':status', 1, PDO::PARAM_INT);
		$query->execute();
	}
	public function updateTask($id, $name, $mail, $description)// изменить задачу
	{
		$sql = 'task set name = :name, email = :email, description = :description, editet = :editet WHERE id = :id';
		$query = $this->update($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':name', $name);
		$query->bindParam(':email', $mail);
		$query->bindParam(':description', $description);
		$query->bindValue(':editet', 1);
		$query->execute();
	}
	private function generateCode($length=6) {// генерация hash кода

		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
	
		$code = "";
	
		$clen = strlen($chars) - 1;  
		while (strlen($code) < $length) {
	
				$code .= $chars[mt_rand(0,$clen)];  
		}
	
		return $code;
	
	}
}
