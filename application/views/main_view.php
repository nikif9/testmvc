<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    <script src="../../js/script.js"></script>
</head>
<body>
    <?php
    if ($data['authorized']) {
        echo '<input type="button" value="log out" class="exit">';
    }else{
        echo '<input type="button" value="log in" class="enter">';
    }
    
    ?>
    <div class="sortBlock">
        <input type="button" value="По имени" class="sort" id="nameSort">
        <input type="button" value="По mail" class="sort" id="mailSort">
        <input type="button" value="По статусу" class="sort" id="statusSort">
        <div class="sortType"></div>
    </div>
    
    <?php
        $view = '';
        foreach ($data['task'] as $key => $task) {
            if ($task['status']) {
                $status = 'Выполнен';
                $class = 'done';
            }else{
                $status = 'Не выполнен';
                $class = 'notDone';
            }
            $view .= '<div class="task" id="'. $task['id']. '">
            <div class="name">'. htmlspecialchars($task['name']). '</div>
            <div class="mail">'. htmlspecialchars($task['email']). '</div>
            <div class="description">'. htmlspecialchars($task['description']) .'</div>
            <div class="editet">';
            if ($task['editet']) {
                $view .=  'Изменено Администратором';
            }
            $view .=  '</div><div class="status '. $class. '">'. $status. '</div>';
            if ($data['authorized']) {
                $view .= '<input type="button" value="Изменить" class="edit">';
                if (!$task['status']) {
                    $view .= '<input type="button" value="Отметить как Выполненое" class="completed">';
                }
            }
            $view .= '</div>';
        }
        $view .= '<div class="pageBlock">';
        for ($i=1; $i <= $data['pageCount']; $i++) { 
            
            $view .= '<div class="page ';
            if ($i == 1) {
                $view .= 'select';
            }
            $view .= '"id="'. $i. '">'. $i. '</div>';
        }
        $view .= '</div>';
        echo $view;
    ?>
    
    <input type="button" value="Добавить задачу" class="addTask">
    <div class="b-popup" style="display: none"> <!-- poup ; -->
        <div class="b-popup-content">
        </div>
    </div> 
    <div class="b-popup-error" style="display: none"> <!-- poup ; -->
        <div class="b-popup-content">
        </div>
    </div> 
</body>
</html>