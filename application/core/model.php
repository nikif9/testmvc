<?php

class Model 
{
	private $mysql;
	const COUNT = 3;
	public function __construct() {
		try {
			$this->mysql = new PDO("mysql:host=mysql.zzz.com.ua;dbname=nikif", 'nikif', 'Password9');
		} catch (PDOException $e) {
			die('Подключение не удалось: ' . $e->getMessage());
		}
	}
	public function getFromTable($string){
		$sql = 'SELECT * FROM '. $string;
		$query = $this->mysql->prepare($sql);
		
		return $query;
	}
	public function pageCount()
	{
		$count = 3;
		$page = 0;
		$query = $this->getFromTable('task LIMIT :offset, :count');
		
		while(true){
			$query->bindValue(':count', $count, PDO::PARAM_INT);
			$query->bindValue(':offset', $page * $count, PDO::PARAM_INT);
			$query->execute();
			if (count($query->fetchAll()) <= 0) {
			break;
			}
			$page++;
			
		}
		return $page;
	}
	public function insert($string){
		$sql = 'INSERT INTO '. $string;
		$query = $this->mysql->prepare($sql);
		return $query;
	}
	public function update($string)
	{
		$sql = 'UPDATE '. $string;
		$query = $this->mysql->prepare($sql);
		return $query;
		
	}
}