start()
$('div:first').remove
function start() {
    curentPage = 1 //начальная страница
    curentSort = null
    admin = false;
    adminCheck();
    function adminCheck() { //проверка на авторизацияю при запуске
        id = getCookie('id');
        hash = getCookie('hash');
        $.ajax({
            url: 'http://dsfdf.zzz.com.ua/script/getHash',
            data:({id: id}),
            async: false,
            type: 'POST',
            success: function (data) {
                data = JSON.parse(data)
                if (data['result'] == 'ok' && data['hash'] == hash) {
                    admin = true
                }
            }
        });
    }
    $(document).on('click','.page',function() { // ивент для отслеживания клика по номеру страницы
        item = $(this)
        curentPage = item.attr('id')
        $('.page').removeClass('select')
        item.addClass('select')
        getPage(curentPage, curentSort)
    })
    $(document).on('click','.b-popup',function() {// ивент для выхода из попапа
        $('.b-popup').hide()
        $('.b-popup-error').hide()
    })
    $(document).on('click','.b-popup .b-popup-content',function(event) {// ивент отмены всплытия
        event.stopPropagation();
    })
    $(document).on('click','.b-popup-error .b-popup-content',function(event) {// ивент отмены всплытия и скрытия уведомления
        event.stopPropagation();
        $('.b-popup-error').hide()
    })
    $(document).on('click','.sort',function() {// ивент для отслежиания клика по кнопакам сортировки и сама сортировка
        item = $(this)
        text = '';
        if (curentSort == item.attr('id') + 'ASC') {
            curentSort = item.attr('id') + 'DESC'
            text = 'Сортировка по убыванию'
        }else{
            curentSort = item.attr('id') + 'ASC'
            text = 'Сортировка по возрастанию'          
        }
        if ($("#default").length == 0) {
            $('.sortType').before('<input type="button" value="отменить сортировку" class="sort" id="default"></input>')
        }
        
        if (item.attr('id') == 'default'){
            curentSort = null;
            text = ''
            $('#default').remove()
        }
        $('.sortType').text(text);
        getPage(curentPage, curentSort)
    
    })
    $(document).on('click','.addTask',function() { // ивент для отслежиания клика по кнопакам добавления задачу и отрисовка попапа
        showPopUp('600px', '300px')
        $('.b-popup .b-popup-content').prepend(
            '<div class=input>\
                Введите  имя:\
                <input type="text" name="" class="addName" required>\
            </div>\
            <div class=input>\
                Введите  мейл:\
                <input type="email" name="mail" class="addMail" required>\
            </div>\
            <div class=input>\
                Введите  описание:\
                <textarea name="Description" class="addDescription" required ></textarea>\
            </div>\
            <input type="button" value="Сохранить" class="save">'
        )
    })
    $(document).on('click','.save',function() {// ивент для отслежиания клика по кнопаке сохранить задачу и логика
        name = $('.addName').val().trim()
        mail = $('.addMail').val().trim()
        description = $('.addDescription').val().trim()
        valide = validate(mail);
        if (name == '') {
            $('.addName').addClass('error')
        }
        if (mail == '' || !valide) {
            $('.addMail').addClass('error')
        }
        if (description == '') {
            $('.addDescription').addClass('error')
        }
        if ($('.error').length == 0) {
            $.ajax({
                url: 'http://dsfdf.zzz.com.ua/script/setTask',
                data:({name: name, mail: mail, description: description}),
                async: false,
                type: 'POST',
                success: function (data) {
                    data = JSON.parse(data);
                    getPage(curentPage, curentSort)// отрисовка задач
                    drawPage(data['pageCount'])// отрисовка страниц
                    $('.b-popup-error .b-popup-content').text('Успешно сохранено');
                    $('.b-popup-error').show()
                    setTimeout("$('.b-popup-error').hide()", 1000);
                    $('.b-popup').hide()
                }
            });
            
        }else{
            if (!valide) {
                $('.b-popup-error .b-popup-content').text('email не валиден');
            }else{
                $('.b-popup-error .b-popup-content').text('Заполни поля');
            }
            $('.b-popup-error').show()
        }
        setTimeout("$('.b-popup-error').hide()", 1000);
    })
    $(document).on('input','.addName, .addMail, .addDescription, .login, .password, editDescription, editMail, editName',function() {
        $(this).removeClass('error');
        $('.b-popup-error').hide()
    })// удаление класса ошибок если они написали что то
    $(document).on('input','.editDescription, .editMail, .editName',function() {
        $('.editName').removeClass('error')
        $('.editMail').removeClass('error')
        $('.editDescription').removeClass('error')
        $('.b-popup-error').hide()
    })// удаление класса ошибок если они написали что то
    function showPopUp(width, height) { // покозать попап
        $('.b-popup .b-popup-content').width(width) 
        $('.b-popup .b-popup-content').height(height) 
        $('.b-popup .b-popup-content').empty();
        $('.b-popup').show()
    }
    function getPage(page, sort) { // получение данных страниц и их отрисовка
        $.ajax({
            url: 'http://dsfdf.zzz.com.ua/script/getPage',
            data:({page: page, sort: sort}),
            async: false,
            type: 'POST',
            success: function (data) {
                task = JSON.parse(data)
                drawTask(task);
            }
        });
    }
    function escapeHtml(text) { // зашита xss уязвимости
        return text
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    }
    function validate(email) { // проверка на валиднсть мейла
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        return reg.test(email);
    }
    function drawTask(data) {// отрисовка задач
        html = ''
        $.each(data, function(index, value){
            if (value['status'] == 0) {
                status = 'Не выполнен'
                statuClass = 'notDone';
            }else{
                status = 'Выполнено'
                statuClass = 'done';             
            }
            console.log(value)
            html += '<div class="task" id="' + value['id'] + '">\
            <div class="name">' + escapeHtml(value['name']) + '</div>\
            <div class="mail">' + escapeHtml(value['email']) + '</div>\
            <div class="description">' + escapeHtml(value['description']) + '</div>\
            <div class="editet">'
            if (value['editet'] == 1) {
                html += 'Изменено Администратором';
            }
            html += '</div><div class="status ' + statuClass + '">' + status + '</div>'
            if (admin == true) {
                html += '<input type="button" value="Изменить" class="edit">'
                if (value['status'] == 0) {
                    html += '<input type="button" value="Отметить как Выполненое" class="completed">'
                }
            }
            html += '</div>'
        })
        console.log(html);
        $('.task').remove()
        $('.sortBlock').after(html)
    }
    function drawPage(count) { // отрисовка блока с номером страниц
        html = '<div class="pageBlock">'
        for (let index = 1; index <= count; index++) {
            html += '<div class="page '
            if (index == curentPage) {
                html += 'select'
            }
            html += '"id="' + index + '">' + index + '</div>';
        }
        html += '</div>'
        console.log(html);
        $('.pageBlock').remove()
        $('.addTask').before(html)
    }
    $(document).on('click','.enter',function(){ // евент по отслеживанию кнопки логин и отрисовка поп апа
        showPopUp('300px', '115px')
        $('.b-popup .b-popup-content').prepend(
            '<div class=input>\
                login:\
            <input type="text" name="" class="login" required>\
            </div>\
            <div class=input>\
                password:\
                <input type="text" name="mail" class="password" required>\
            </div>\
            <input type="button" value="Log in" class="submit">'
        )
    })
    $(document).on('click','.submit',function() { // авторищация
        login = $('.login').val().trim()
        password = $('.password').val().trim()
        if (login == '') {
            $('.login').addClass('error')
        }
        if (password == '') {
            $('.password').addClass('error')
        }
        if ($('.error').length == 0) {
            $.ajax({
                url: 'http://dsfdf.zzz.com.ua/script/login',
                data:({login: login, password: password}),
                async: false,
                type: 'POST',
                success: function (data) {
                    data = JSON.parse(data)
                    console.log(data);
                    if (data['result'] == 'ok') {
                        document.cookie = "id=" + data['id'] + "; max-age=" + 60*60*24*30;
                        document.cookie = "hash=" + data['hash'] + "; max-age=" + 60*60*24*30;
                        $('.enter').remove();
                        $('body').prepend('<input type="button" value="log out" class="exit">')
                        $('.task').append('<input type="button" value="Изменить" class="edit"> <input type="button" value="Отметить как Выполненое" class="completed">')
                        admin = true;
                    }else{
                        $('.b-popup-error .b-popup-content').text('Неправильные данные');
                        $('.b-popup-error').show()
                    }
                    // drawTask(data['task'])
                    // drawPage(data['pageCount'])
                    // console.log(data)
                    $('.b-popup').hide()
                }
            });
            
        }else{
            $('.b-popup-error .b-popup-content').text('Заполни поля');
            $('.b-popup-error').show()
        }
        setTimeout("$('.b-popup-error').hide()", 1000);
    })
    $(document).on('click','.exit',function() { // выйти изз аккаунта
        $.ajax({
            url: 'http://dsfdf.zzz.com.ua/script/logout',
            data:({id: id}),
            async: false,
            type: 'POST',
            success: function (data) {
                document.cookie = "id=null;max-age=-1"
                document.cookie = "hash=null;max-age=-1"
                $('.exit, .edit, .completed').remove();
                admin = false;
                $('body').prepend('<input type="button" value="log in" class="enter">')
            }
        });
        
    })
    $(document).on('click','.completed',function() {// отслежиания кнопки отметить как выполнено
        id = $(this).parent().attr('id')
        hash = getCookie('hash');
        $.ajax({
            url: 'http://dsfdf.zzz.com.ua/script/completed',
            data:({id: id, hash: hash}),
            async: false,
            type: 'POST',
            success: function (data) {
                data = JSON.parse(data)
                if (data['result'] == 'ok') {
                    getPage(curentPage, curentSort)
                }else{
                    $('.b-popup-error .b-popup-content').text('Ошибка при авторизации')
                    $('.b-popup-error').show()
                    setTimeout("$('.b-popup-error').hide()", 1000);
                } 
            }
        });
    })
    $(document).on('click','.edit',function() {// отслежиания кнопки изменить и отрисовка попапа
        task = $(this).parent()
        id = task.attr('id')
        name = task.children('.name').text().trim();
        mail = task.children('.mail').text().trim();
        description = task.children('.description').text().trim();
        showPopUp('600px', '300px')
        $('.b-popup .b-popup-content').prepend(
            '<div class=input>\
                Имя:\
                <input type="text" name="" class="editName" required>\
            </div>\
            <div class=input>\
                Мейл:\
                <input type="email" name="mail" class="editMail" required>\
            </div>\
            <div class=input>\
                Описание:\
                <textarea name="Description" class="editDescription" required ></textarea>\
            </div>\
            <input type="button" value="сохранить" class="editSave">'
        )
        $('.editName').val(name);
        $('.editMail').val(mail);
        $('.editDescription').val(description);
        $(document).on('click','.editSave',function() {
            changed = false;
            changedName = $('.editName').val().trim()
            changedMail = $('.editMail').val().trim()
            changedDescription = $('.editDescription').val().trim()
            hash = getCookie('hash')
            valide = validate(changedMail);
            if (changedName == '' ) {
                $('.editName').addClass('error')
            }
            if (changedMail == '' || !valide ) {
                $('.editMail').addClass('error')
            }
            if (changedDescription == '') {
                $('.editDescription').addClass('error')
            }
            if (changedDescription != description || changedMail != mail || changedName != name) {
                changed = true;
            }
            if (!changed) {
                $('.editName').addClass('error')
                $('.editMail').addClass('error')
                $('.editDescription').addClass('error')
            }
            if ($('.error').length == 0 && changed) {
                $.ajax({
                    url: 'http://dsfdf.zzz.com.ua/script/editTask',
                    data:({id: id, name: changedName, mail: changedMail, description: changedDescription, hash: hash}),
                    async: false,
                    type: 'POST',
                    success: function (data) {
                        data = JSON.parse(data)
                        if (data['result'] == 'ok') {
                            getPage(curentPage, curentSort)
                            $('.b-popup-error .b-popup-content').text('Успешно изменено');
                            $('.b-popup-error').show()
                            setTimeout("$('.b-popup-error').hide()", 1000);
                        }else{
                            $('.b-popup-error .b-popup-content').text('Ошибка при авторизации')
                            $('.b-popup-error').show()
                            setTimeout("$('.b-popup-error').hide()", 1000);
                        }  
                        $('.b-popup').hide()
                    }
                });
                
            }else{
                if (!valide) {
                    $('.b-popup-error .b-popup-content').text('Email не валиден');
                }else if (!changed) {
                    $('.b-popup-error .b-popup-content').text('Изменить значения поля');
                }else{
                    $('.b-popup-error .b-popup-content').text('Заполни поля')
                }
                $('.b-popup-error').show()
            }
            setTimeout("$('.b-popup-error').hide()", 1000);
        })
    })
    function getCookie(name) { // получение куков
        let matches = document.cookie.match(new RegExp(
          "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
      }
}
